/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author rodrigorivera
 */
public class Cliente {

    public static void main(String[] args) throws IOException {
        byte mensaje[] = new byte[1024];

        BufferedReader sc = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.print("a que dirreccion te quieres conectar: ");
        String ip = sc.readLine();
        System.out.print("a que puerto: ");
        int puerto = Integer.parseInt(sc.readLine());
        try {
            Socket soket = new Socket(ip, puerto);
            DataInputStream din = new DataInputStream(soket.getInputStream()); // recibir coasas 
            DataOutputStream dos = new DataOutputStream(soket.getOutputStream());// eviar coasas

            System.out.println("-------Cliente-------");
            System.out.print("Introcue codigo a enviar:");
            String sMensaje = sc.readLine();
            dos.write(sMensaje.getBytes());// envia 
            /**
             * recive datos de la imagen
             */
            // recibir mensaje de espesifiaciones 
            din.read(mensaje, 0, mensaje.length);

            byte code = mensaje[0];
            byte idPok = mensaje[1];
            byte[] tam = sacarTamaño(mensaje);
            int numPaketes = byteAInt(tam); // numeor de paquetes
            byte[] imagenByte = new byte[numPaketes];

            tam = sacarTamPack(mensaje);
            numPaketes = byteAInt(tam);
            System.out.println("tamaño pack " + numPaketes);
            String a1 = new String(mensaje).trim();

            System.out.println("espesificacioens de el ms: " + a1);
            System.out.println("-------Servidor-------");
            System.out.println("Codigo recibido: " + code);
            String temp = "" + code;
            System.out.println("Codigo recibido: " + temp.getBytes().length);
            System.out.println("Id Pokemon " + idPok);
            System.out.println("Respuesta 1 : " + sacarRes1(mensaje));
            System.out.println("Respuesta 2 : " + sacarRes2(mensaje));
            System.out.println("respuesta 3 :" + sacarRes3(mensaje));
            System.out.println("recibiendo imagen...");
            System.out.println("paquetes recibidos : " + numPaketes);
            System.out.println("tamaño de la imagen en bytes: " + imagenByte.length);

            int pivete = 0;

            for (int i = 0; i < numPaketes; i++) {
                mensaje = new byte[1024];
                din.read(mensaje, 0, mensaje.length);
                for (int j = 0; j < mensaje.length; j++) {
                    imagenByte[pivete] = mensaje[j];
                  //      System.out.println(pivete + " _._ " + mensaje[j]);
                    pivete++;
                }

            }
           
            crarImagen(imagenByte);
            System.out.println("imgen recibida y guardada correctamente ");
            System.out.println("abriendo imgen ....");

            

            /**
             * esccribir y enviar, la respuesta de la trivia
             */
            System.out.println("-------Cliente-------");
            System.out.print("Indrodue el numero de respuesta : ");
            Scanner s = new Scanner(System.in);
            byte a11 = s.nextByte();
            System.out.println("texbytes : " + a11 + "..");
            code = 11;
            // construccion de el paquete 
            mensaje[0] = code;
            mensaje[1] = a11;
            dos.write(mensaje);
            /**
             * mesaje de el servido saeber si esta bien o mal
             */
            din.read(mensaje, 0, mensaje.length);
            if (mensaje[1] == 1) {
                System.out.println("-------Servidor-------");
                System.out.println("Codigo recibido: " + mensaje[0]);
                System.out.println("rxbyte: " + (mensaje[0] + "").getBytes().length);
                System.out.println("la respuesta es correcta");
            } else {
                System.out.println("-------Servidor-------");
                System.out.println("Codigo recibido: " + mensaje[0]);
                System.out.println("rxbyte: " + (mensaje[0] + "").getBytes().length);
                System.out.println("la respuesta es incorrecta");

            }
        } catch (Exception e) {
            System.out.println("ocurrio un error");
            System.exit(-1);
        }

    }

    private static int byteAInt(byte[] b) {
        final ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        return bb.getInt();
    }

    private static byte[] sacarTamaño(byte[] ca) {
        byte[] tam = new byte[4];
        int j = 0;
        for (int i = 2; i < 6; i++) {
            tam[j] = ca[i];
            j++;
        }

        return tam;
    }

    private static String sacarRes1(byte[] ca) {
        String cadena;
        byte as[] = new byte[15];
        int j = 0;
        for (int i = 6; i < 21; i++) {
            as[j] = ca[i];
            j++;
        }
        cadena = new String(as).trim();
        return cadena;
    }

    private static String sacarRes2(byte[] ca) {
        String cadena;
        byte as[] = new byte[15];
        int j = 0;
        for (int i = 21; i < 36; i++) {
            as[j] = ca[i];
            j++;
        }
        cadena = new String(as).trim();
        return cadena;
    }

    private static String sacarRes3(byte[] ca) {
        String cadena;
        byte as[] = new byte[15];
        int j = 0;
        for (int i = 36; i < 51; i++) {
            as[j] = ca[i];
            j++;
        }
        cadena = new String(as).trim();
        return cadena;
    }

    private static byte[] sacarTamPack(byte[] ca) {
        System.out.println("tamaño de el " + ca.length);
        byte[] tam = new byte[4];
        int j = 0;
        for (int i = 51; i < 55; i++) {
            tam[j] = ca[i];
            j++;
        }
        return tam;

    }

    public static void crarImagen(byte[] arreglo) {
        FileOutputStream fileOuputStream = null;
        try {
            fileOuputStream = new FileOutputStream("imgenRecibida.png");
            fileOuputStream.write(arreglo);
            fileOuputStream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileOuputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
