/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

/**
 *
 * @author rodrigorivera
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rodrigorivera
 */
public class Imgen {

    private BufferedImage image;
    private byte idArreglo[] = {26, 22, 1};
    private String nombreArreglo[] = {"uno", "dos", "tres"};
    private String[] nombresPokemon = {"Raichu", "Fearow", "bulbasaur"};
    private String nombrePokemon;
    private byte id;
    private String nombreImg;
    private int tamaño;
    private int numero;

    public Imgen() {
        this.numero = ramdom();
    }

    public byte[] obetenerImagen() {
        
        id = idArreglo[numero];
        nombrePokemon = nombresPokemon[numero];
        nombreImg = nombreArreglo[numero];
        FileInputStream fileInputStream = null;
        File imgen = new File(nombreImg + ".png");
        byte[] arreglo = new byte[(int) imgen.length()]; //tamaño imge
        tamaño = arreglo.length;
        

        try {
            fileInputStream = new FileInputStream(imgen);
            fileInputStream.read(arreglo);
            fileInputStream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("cai en errror ");
        } catch (IOException ex) {
            Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("cai en error");
        }
  
        return arreglo;
    }

    /**
     * crear Imgen 
     * @param arreglo 
     */
    public void crarImagen(byte[] arreglo) {
        FileOutputStream fileOuputStream = null;
        try {
            fileOuputStream = new FileOutputStream("imgen.png");
            fileOuputStream.write(arreglo);
            fileOuputStream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileOuputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Imgen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private String stringImg(byte[] img) {
        String a = "";
        for (int i = 0; i < img.length; i++) {
            a += img[i] + ".";
            
        }
        
        return a;
    }

    private int ramdom() {
        //generador de numeros aleatorios
        Random generadorAleatorios = new Random();

        //genera un numero entre 1 y 5 y lo guarda en la variable numeroAleatorio
        int numeroAleatorio = 0 + generadorAleatorios.nextInt(3);
        //imprimo el numero en consola
        
        return numeroAleatorio;

    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public byte[] getIdArreglo() {
        return idArreglo;
    }

    public void setIdArreglo(byte[] idArreglo) {
        this.idArreglo = idArreglo;
    }

    public String[] getNombreArreglo() {
        return nombreArreglo;
    }

    public void setNombreArreglo(String[] nombreArreglo) {
        this.nombreArreglo = nombreArreglo;
    }

    public String[] getNombresPokemon() {
        return nombresPokemon;
    }

    public void setNombresPokemon(String[] nombresPokemon) {
        this.nombresPokemon = nombresPokemon;
    }

    public String getNombrePokemon() {
        return nombrePokemon;
    }

    public void setNombrePokemon(String nombrePokemon) {
        this.nombrePokemon = nombrePokemon;
    }

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getNombreImg() {
        return nombreImg;
    }

    public void setNombreImg(String nombreImg) {
        this.nombreImg = nombreImg;
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

}
