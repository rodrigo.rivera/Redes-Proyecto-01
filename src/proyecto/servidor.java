/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rodrigorivera
 */
public class servidor extends Thread {

    private Socket socket = null;

    public servidor(Socket socket) {
        super("KKMultiServerThread");
        this.socket = socket;
    }

    public void run() {
        String m = "hola cleinte soy un servidor";
        String a = "pokemon";
        System.out.println("pokemon: " + a.getBytes().length);
        byte[] mensaje = new byte[1024];

        System.out.println("-------Servidor-------");
        System.out.println("IP de el Cliente : " + socket.getLocalAddress());
        System.out.println("Puerto de el Cliente: " + socket.getLocalPort());
        System.out.println("con el hilo : " + this.getId());
        Protocolo protocolo = new Protocolo();
        while (true) {

            try {

                // datos para enviar 
                DataInputStream dis = new DataInputStream(socket.getInputStream());// entra
                DataOutputStream dos = new DataOutputStream(socket.getOutputStream()); // sale

                // mesaje recibido de el cliente 
                dis.read(mensaje, 0, mensaje.length); // mensaje recivido
                String men = new String(mensaje).trim();
                System.out.println("-------Cliente-------");
                System.out.println("Codigo recibido: " + men);
                System.out.println("rxbyte: " + men.getBytes().length);
                /**
                 * mesaje de espesificaciones de la imagen codigo -
                 * idpokeon-res1-res2-res3-numPackImg
                 */
                // declaracion de varibles de el paquete 
                Imgen img = new Imgen();
                int respuestaTrivia = img.getNumero() + 1;
                byte[] imgByte = img.obetenerImagen();
                byte codigo = 20;
                byte idPokeom = img.getId();
                int tamaño = img.getTamaño();
                int numPaquetes = (tamaño / mensaje.length) ;
                byte[] tamañoPack = new byte[4];
                byte[] tamañoBytes = new byte[4];
                //llenar esa varibles de el paquete 
                tamañoPack = intAByte(numPaquetes);
                tamañoBytes = intAByte(tamaño);

                byte[] respuesta1 = img.getNombresPokemon()[0].getBytes();
                byte[] respuesta2 = img.getNombresPokemon()[1].getBytes();
                byte[] respuesta3 = img.getNombresPokemon()[2].getBytes();

                //esta dfinido el tamño de cada paquete en el metodo crearpakete
                mensaje = crearPakete(codigo, idPokeom, tamañoBytes, respuesta1, respuesta2, respuesta3, tamañoPack);
                men = "" + codigo;
                dos.write(mensaje);
                System.out.println("-------Servidor-------");
                System.out.println("tamño de la imgen: " + tamaño + " bytes");
                System.out.println("codigo enviado: " + mensaje[0]);
                System.out.println("texbyte: " + men.getBytes().length);
                System.out.println("enviando img.... ");
                int pivete = 0;
                
                for (int i = 0; i < numPaquetes; i++) {
                    mensaje = new byte[1024];
                    for (int j = 0; j < mensaje.length; j++) {
                        mensaje[j] = imgByte[pivete];
                        //System.out.println(pivete+" --:-- "+mensaje[j]);
                        pivete++;
                    }
                    dos.write(mensaje);

                }
                System.out.println("paquetes enviados: " + numPaquetes);
                System.out.println("txbytes: " + imgByte.length + " imagen");

                /**
                 * recibir la respuesta de la trivia
                 */
                System.out.println("ress:" + respuestaTrivia);
                dis.read(mensaje, 0, mensaje.length); // lo guardo en la varible
                men = mensaje[0] + "";
                System.out.println("-------Cliente-------");
                System.out.println("codigo recibido " + mensaje[0]);
                System.out.println("rxbytes : " + men.getBytes().length);
                System.out.println("idPokemon " + img.getIdArreglo()[mensaje[1] - 1]);
                System.out.println("id Respuesta " + mensaje[1]);

                //enviar paquete 
                byte respuestaCliente = mensaje[1];

                if (respuestaTrivia == respuestaCliente) {
                    codigo = 21;
                    mensaje[0] = codigo;
                    mensaje[1] = 1;
                    dos.write(mensaje);
                    System.out.println("-------Servidor-------");
                    System.out.println("respuesta correcta!");
                    System.out.println("codigo enviado " + mensaje[0]);
                    System.out.println("txbyte: " + (mensaje[0] + "").getBytes().length);
                    System.out.println("cerrando conexion");

                } else { // se equivoco 
                    codigo = 21;
                    mensaje[0] = codigo;
                    mensaje[1] = 0;
                    dos.write(mensaje);
                    System.out.println("-------Servidor-------");
                    System.out.println("respuesta Incorrecta!");
                    System.out.println("codigo enviado " + mensaje[0]);
                    System.out.println("txbyte: " + (mensaje[0] + "").getBytes().length);
                    System.out.println("cerrando conexion");

                }

                
                
                break;
            } catch (IOException ex) {
                Logger.getLogger(servidor.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private byte[] intAByte(int i) {
        final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(i);
        return bb.array();
    }

    private byte[] crearPakete(byte codigo, byte idPokemon, byte[] tamaño,
            byte[] respuesta1, byte[] respuesta2, byte[] respuesta3, byte[] tamñoPa) {

        byte tempCrearPakete[] = new byte[55];

        tempCrearPakete[0] = codigo;
        tempCrearPakete[1] = idPokemon;
        //tamño
        int j = 0;
        int k = 0;
        // tamañp
        int y = 2;
        int r = 6;
        while (k < tamaño.length) {
            tempCrearPakete[y] = tamaño[k];
            k++;
            y++;
        }

        //respuesta 1
        //6; i < 21
        y = 6;
        k = 0;
        while (k < respuesta1.length) {
            tempCrearPakete[y] = respuesta1[k];
            k++;
            y++;
        }

        //respuesta 2
        //21; i < 36
        y = 21;
        k = 0;

        while (k < respuesta2.length) {
            tempCrearPakete[y] = respuesta2[k];
            k++;
            y++;
        }

        //respuesta 3
        y = 36;
        k = 0;

        while (k < respuesta3.length) {
            tempCrearPakete[y] = respuesta3[k];
            k++;
            y++;
        }
        // numero de paquetes
        y = 51;
        k = 0;

        while (k < tamñoPa.length) {
            tempCrearPakete[y] = tamñoPa[k];
            k++;
            y++;
        }
//        System.out.println("mostrar");
//        for (int i = 0; i < tempCrearPakete.length; i++) {
//            System.out.println(i + "-" + tempCrearPakete[i]);
//
//        }

        return tempCrearPakete;
    }

    private int byteAInt(byte[] b) {
        final ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        return bb.getInt();
    }

}
