/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;

/**
 *
 * @author rodrigorivera
 */
public class runServidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        boolean listening = true;
        Scanner sc= new Scanner(System.in);
        System.out.print("escoje un puerto en el cual escucharas:");
        int pueto = sc.nextInt();
        ServerSocket serverSocket = new ServerSocket(pueto);
        System.out.println("esperando clientes.....");
        while (listening) {
            new servidor(serverSocket.accept()).start();
        }

        serverSocket.close();
    }

}
